import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.Charset;
import java.util.Base64;

public class Main {

    public static void main(String[] args) {




       int args_lenth = 0;

        if(args.length > 0){
            args_lenth = args.length;
        }

        switch (args_lenth){
            case 0 :
                System.out.println("Túl kevés paraméter!");break;
            case 1 :
                System.out.println("Túl kevés paraméter"); break;
            case 2 :
                System.out.println(decrypt(args[0],args[1]));
                break;
            default :
                System.out.println("Túl sok paraméter értéket adott meg"); break;
        }



    }


    public static String decrypt(String key_,String encryptedText) {

        byte[] key = key_.getBytes(Charset.forName("UTF-8"));
        try {
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
            SecretKeySpec secretKey = new SecretKeySpec(key, "AES");
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            byte[] cipherText = Base64.getDecoder().decode(encryptedText.getBytes("UTF8"));
            String decryptedString = new String(cipher.doFinal(cipherText),"UTF-8");
            return decryptedString;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
